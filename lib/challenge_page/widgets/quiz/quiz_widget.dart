import 'package:DevQuiz/challenge_page/widgets/awnser_widget/awnser_wdiget.dart';
import 'package:DevQuiz/core/app_text_styles.dart';
import 'package:flutter/material.dart';

class QuizWidget extends StatelessWidget {
  final String title;

  QuizWidget(this.title);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Text(
            title, style: AppTextStyles.heading,
          ),
          AwnserWidget("Possibilita a criação de aplicativos compilados nativamente", false, false)
        ],
      ),
    );
  }
}