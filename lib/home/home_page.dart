import 'package:DevQuiz/widgets/app_bar_widget.dart';
import 'package:DevQuiz/widgets/level_button/level_button.dart';
import 'package:DevQuiz/widgets/quiz_card/QuizCardWidget.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarWidget(),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: Column(
            children: [
              SizedBox(
                height: 24
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                  LevelButtonWidget("Fácil"),
                  LevelButtonWidget("Médio"),
                  LevelButtonWidget("Difícil"),
                  LevelButtonWidget("Perito"),
                ],),
              ),
              SizedBox(
                height: 24
              ),
              Expanded(child: GridView.count(
                crossAxisSpacing: 16,
                crossAxisCount: 2,
                mainAxisSpacing: 16,
                children: [
                  QuizCardWidget(),
                  QuizCardWidget(),
                  QuizCardWidget()
                ],
              ))

            ],
          ),
      ),
    );
  }
}